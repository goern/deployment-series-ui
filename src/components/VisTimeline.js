import React from 'react';
import ReactDOM from 'react-dom';
import EventBus from 'vertx3-eventbus-client'
import vis from 'vis';
import FontAwesome from 'react-fontawesome';

const mockProjects = [
    {
        'id': '"default"',
        'content': 'OpenShift'
    }
]

const mockDeployments = [
    {
        'id': '1',
        'group': 'default',
        'content': 'deployment-1',
        'title': 'super duper deployment #1, go <a href="http://openshift.com/">home</a>!',
        'start': '2017-07-17T01:20:00',
        'end': '2017-07-17T01:37:00'
    }
]

class GroupTemplate extends React.Component {
    render() {
        var { group } = this.props;
        return <div>
            <label>{group.content}</label>
        </div>
    }
}

class ItemTemplate extends React.Component {
    render() {
        var { item } = this.props;
        return <div>
            <label>{item.content}</label>
        </div>
    }
}

class VisTimeline extends React.Component {
    constructor() {
        super();
        this.eventBus = null;
        this.numProjects = 3;
    }

    componentDidMount() {
        this.eventBus = new EventBus('http://deployment-series-server-myproject.127.0.0.1.nip.io/eventbus');
        const eb = this.eventBus;

        var container = document.getElementById('deployments');
        var items = new vis.DataSet();
        var groups = new vis.DataSet()

        var self = this;

        var options = {
            orientation: 'top',
            maxHeight: 600,
            editable: false,
            tooltipDelay: 750,
            template: function (item, element) {
                if (!item) { return }
                ReactDOM.unmountComponentAtNode(element);
                return ReactDOM.render(<ItemTemplate item={item} />, element);
            },
            groupTemplate: function (group, element) {
                if (!group) { return }
                ReactDOM.unmountComponentAtNode(element);
                return ReactDOM.render(<GroupTemplate group={group} />, element);
            }
        }

        eb.onopen = function () {
            eb.registerHandler('projects', (err, message) => {
                if (!err) {
                    groups.clear();
                    groups.add(message.body);
                }
            });

            eb.registerHandler('builds', (err, message) => {
                if (!err) {
                    items.clear();
                    items.add(message.body);
                    items.add(mockDeployments);
                }
            });

        }

        var timeline = new vis.Timeline(container, items, groups, options);
        timeline.fit();
    }

    render() {
        return <div className="openshift">
            <h1>OpenShift Deployments</h1>
            <div id="deployments"></div>
        </div>
    }
}

export default VisTimeline;
