import React from 'react';
import EventBus from 'vertx3-eventbus-client'
import { Logger, ConsoleLogger } from 'react-console-logger';


class EventLog extends React.Component {
    constructor() {
        super();
        this.state = {
            data: {
                messages: []
            }
        };
        this.eventBus = null;
        this.eventLogger = new Logger();
    }

    componentDidMount() {
        this.eventBus = new EventBus('http://deployment-series-server-myproject.127.0.0.1.nip.io/eventbus');
        const eb = this.eventBus;
        const self = this;

        eb.onopen = function () {
            eb.registerHandler('eventrouter', (err, message) => {
                if (!err) {
                    const messages = self.state.data.messages;

                    self.eventLogger.log(message.body);

                    messages.push(message.body);
                    self.setState({
                        data: {
                            messages
                        }
                    })

                }
            });
        }
    }

    render() {
        const messages = this.state.data.messages;

        if (messages) {
            messages.map(message_object => {
                if (message_object) {
                    return (
                        <li>{message_object.message}</li>
                    )
                }
            })
        }

        return <div>
            <h2>EventLog</h2>
            <ConsoleLogger logger={this.eventLogger} style={{
                width: 800,
                display: 'flex',
                margin: 'auto'
            }} />
        </div>
    }
}

export default EventLog;
