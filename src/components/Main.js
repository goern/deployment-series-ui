require('normalize.css/normalize.css');
require('styles/App.css');
require('vis/dist/vis.css');

import React from 'react';
import VisTimeline from  './VisTimeline.js';

class AppComponent extends React.Component {
  render() {
    return (
      <div className="index">
        <VisTimeline />
        <footer>
          <p>Released under GPL-3.0 license, more details on our <a href="https://gitlab.com/goern/">Git repository</a>.</p>
        </footer>
        </div>
    );
  }
}

AppComponent.defaultProps = {
};

export default AppComponent;
