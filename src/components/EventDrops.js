import React from 'react';

import d3 from 'd3';
import eventDrops from 'event-drops';


// const colors = d3.schemeCategory10;

const FONT_SIZE = 16; // in pixels
const TOOLTIP_WIDTH = 30; // in rem

class EventDrops extends React.Component {
    constructor(props) {
        super(props)
        this.createChart = this.createChart.bind(this)
    }

    createChart() {
        this.chart = eventDrops()
            .start(new Date(new Date().getTime() - (3600000 * 24 * 365))) // one year ago
            .end(new Date())
            .eventLineColor((d, i) => colors[i])
            .date(d => new Date(d.date))
            .mouseover(showTooltip)
            .mouseout(hideTooltip)
            .zoomend(renderStats);
    }

    render() {
        return (
            <div>
                <h1>OpenShift Deployments Drops</h1>
                <div id="eventdrops"></div>
                <p className="infos">
                    <span id="numberCommits"></span> deployments found between <span id="zoomStart"></span>
                    and <span id="zoomEnd"></span>.
            </p>
            </div>
        )
    }
}

export default EventDrops;
